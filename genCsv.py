#!/bin/env python

import argparse
import collections
import csv
import requests
import StringIO

import tmEventSetup


url = 'https://docs.google.com/spreadsheet/ccc?key=1JroeAWb0zU6UVyHBXPIevgyIXnxgHyV7ZNlLmLBfwSY&output=csv'
xml = 'L1Menu_Collisions2016_v5.xml'
output = 'prescale.csv'

parser = argparse.ArgumentParser()

parser.add_argument("--menu", dest="xml", default=xml, type=str, action="store", required=True, help="path to the level1 trigger menu xml file")
parser.add_argument("--prescale", dest="url", default=url, type=str, action="store", help="url for the prescale google doc")
parser.add_argument("--output", dest="output", default=output, type=str, action="store", help="output file name")

options = parser.parse_args()


response = requests.get(options.url)
response.raise_for_status()

io = StringIO.StringIO(response.content)
reader = csv.DictReader(io)

pscolumns = {}
for header in reader.fieldnames:
  try:
    lumi = float(header)
    pscolumns[header] = 0
  except:
    continue

od = collections.OrderedDict(sorted(pscolumns.items(), key=lambda x: float(x[0]), reverse=True))
idx = 1
for key, value in od.iteritems():
  od[key] = idx
  idx += 1

menu = tmEventSetup.getTriggerMenu(options.xml)
algoMap = menu.getAlgorithmMapPtr()
algorithms = {}
for key, algo in algoMap.iteritems():
  algorithms[algo.getName()] = algo.getIndex()


header = '# <algo id>,  <veto mask>, <finor mask>'
index =  '         -1,           -3,           -2'
for key, value in od.iteritems():
  header += ', <%s>' % key
  index += ',          %s' % value

from requests.structures import CaseInsensitiveDict
used = CaseInsensitiveDict(algorithms)

lines = {}
for row in reader:
  seed = row['L1SeedName']
  if not seed.startswith('L1_'): continue
  prescales = {}
  idx = None
  try:
    idx = used[seed]
  except:
    print 'err> not defined in xml menu: %s' % seed
    continue

  used[seed] = 'ok'
  line =  '        %3s,            0,            1' % idx
  for key, value in od.iteritems():
    line += ',%11s' % row[key]
  lines[idx] = line


fp = file(options.output, 'wb')
fp.write('%s\n' % header)
fp.write('%s\n' % index)
for key, value in sorted(lines.items()):
  fp.write('%s\n' % value)

for key, value in used.iteritems():
  if value != 'ok':
    print 'war> not defined in google doc: %s' % key

# eof
