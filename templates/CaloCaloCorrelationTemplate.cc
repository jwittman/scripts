{#
 # @author: Takashi MATSUSHITA
 #}
{% block CaloCaloCorrelationTemplate scoped %}

{% import 'macros.jinja2' as macros %}

{% set objects = cond.getObjects() %}
{% set nObjects = objects | length %}
{% set objects = objects[0] | sortObjects(objects[1]) %}

{% if objects[0].getType() == tmEventSetup.Egamma %}
  {% set nEtaBits0 = scaleMap[tmGrammar.EG + '-' + tmGrammar.ETA].getNbits() %}
  {% set nPhiBits0 = scaleMap[tmGrammar.EG + '-' + tmGrammar.PHI].getNbits() %}
  {% set phiScale0 = scaleMap[tmGrammar.EG + '-' + tmGrammar.PHI] %}
  {% set prefix0 = prefixEg %}

  {% if objects[1].getType() == tmEventSetup.Egamma %}
    {% set nEtaBits1 = nEtaBits0 %}
    {% set nPhiBits1 = nPhiBits0 %}
    {% set phiScale1 = phiScale0 %}
    {% set prefix1 = prefix0 %}
    {% set precision = 2*scaleMap['PRECISION-' + tmGrammar.EG + '-' + tmGrammar.EG + '-MassPt'].getNbits()
                     +   scaleMap['PRECISION-' + tmGrammar.EG + '-' + tmGrammar.EG + '-Math'].getNbits() %}
    {% set LUTS = {'DPHI': 'LUT_DPHI_EG_EG',
                   'DETA': 'LUT_DETA_EG_EG',
                   'COS_DPHI': 'LUT_COS_DPHI_EG_EG',
                   'COSH_DETA': 'LUT_COSH_DETA_EG_EG',
                   'ET0': 'LUT_EG_ET',
                   'ET1': 'LUT_EG_ET',
                   'PREC_MASS': precision
                  } %}

  {% elif objects[1].getType() == tmEventSetup.Jet %}
    {% set nEtaBits1 = scaleMap[tmGrammar.JET + '-' + tmGrammar.ETA].getNbits() %}
    {% set nPhiBits1 = scaleMap[tmGrammar.JET + '-' + tmGrammar.PHI].getNbits() %}
    {% set phiScale1 = scaleMap[tmGrammar.JET + '-' + tmGrammar.PHI] %}
    {% set prefix1 = prefixJet %}
    {% set precision = 2*scaleMap['PRECISION-' + tmGrammar.EG + '-' + tmGrammar.JET + '-MassPt'].getNbits()
                     +   scaleMap['PRECISION-' + tmGrammar.EG + '-' + tmGrammar.JET + '-Math'].getNbits() %}
    {% set LUTS = {'DPHI': 'LUT_DPHI_EG_JET',
                   'DETA': 'LUT_DETA_EG_JET',
                   'COS_DPHI': 'LUT_COS_DPHI_EG_JET',
                   'COSH_DETA': 'LUT_COSH_DETA_EG_JET',
                   'ET0': 'LUT_EG_ET',
                   'ET1': 'LUT_JET_ET',
                   'PREC_MASS': precision
                  } %}

  {% elif objects[1].getType() == tmEventSetup.Tau %}
    {% set nEtaBits1 = scaleMap[tmGrammar.TAU + '-' + tmGrammar.ETA].getNbits() %}
    {% set nPhiBits1 = scaleMap[tmGrammar.TAU + '-' + tmGrammar.PHI].getNbits() %}
    {% set phiScale1 = scaleMap[tmGrammar.TAU + '-' + tmGrammar.PHI] %}
    {% set prefix1 = prefixTau %}
    {% set precision = 2*scaleMap['PRECISION-' + tmGrammar.EG + '-' + tmGrammar.TAU + '-MassPt'].getNbits()
                     +   scaleMap['PRECISION-' + tmGrammar.EG + '-' + tmGrammar.TAU + '-Math'].getNbits() %}
    {% set LUTS = {'DPHI': 'LUT_DPHI_EG_TAU',
                   'DETA': 'LUT_DETA_EG_TAU',
                   'COS_DPHI': 'LUT_COS_DPHI_EG_TAU',
                   'COSH_DETA': 'LUT_COSH_DETA_EG_TAU',
                   'ET0': 'LUT_EG_ET',
                   'ET1': 'LUT_TAU_ET',
                   'PREC_MASS': precision
                  } %}
  {% endif %}

{% elif objects[0].getType() == tmEventSetup.Jet %}
  {% set nEtaBits0 = scaleMap[tmGrammar.JET + '-' + tmGrammar.ETA].getNbits() %}
  {% set nPhiBits0 = scaleMap[tmGrammar.JET + '-' + tmGrammar.PHI].getNbits() %}
  {% set phiScale0 = scaleMap[tmGrammar.JET + '-' + tmGrammar.PHI] %}
  {% set prefix0 = prefixJet %}

  {% if objects[1].getType() == tmEventSetup.Jet %}
    {% set nEtaBits1 = nEtaBits0 %}
    {% set nPhiBits1 = nPhiBits0 %}
    {% set phiScale1 = phiScale0 %}
    {% set prefix1 = prefix0 %}
    {% set precision = 2*scaleMap['PRECISION-' + tmGrammar.JET + '-' + tmGrammar.JET + '-MassPt'].getNbits()
                     +   scaleMap['PRECISION-' + tmGrammar.JET + '-' + tmGrammar.JET + '-Math'].getNbits() %}
    {% set LUTS = {'DPHI': 'LUT_DPHI_JET_JET',
                   'DETA': 'LUT_DETA_JET_JET',
                   'COS_DPHI': 'LUT_COS_DPHI_JET_JET',
                   'COSH_DETA': 'LUT_COSH_DETA_JET_JET',
                   'ET0': 'LUT_JET_ET',
                   'ET1': 'LUT_JET_ET',
                   'PREC_MASS': precision
                  } %}

  {% elif objects[1].getType() == tmEventSetup.Tau %}
    {% set nEtaBits1 = scaleMap[tmGrammar.TAU + '-' + tmGrammar.ETA].getNbits() %}
    {% set nPhiBits1 = scaleMap[tmGrammar.TAU + '-' + tmGrammar.PHI].getNbits() %}
    {% set phiScale1 = scaleMap[tmGrammar.TAU + '-' + tmGrammar.PHI] %}
    {% set prefix1 = prefixTau %}
    {% set precision = 2*scaleMap['PRECISION-' + tmGrammar.JET + '-' + tmGrammar.TAU + '-MassPt'].getNbits()
                     +   scaleMap['PRECISION-' + tmGrammar.JET + '-' + tmGrammar.TAU + '-Math'].getNbits() %}
    {% set LUTS = {'DPHI': 'LUT_DPHI_JET_TAU',
                   'DETA': 'LUT_DETA_JET_TAU',
                   'COS_DPHI': 'LUT_COS_DPHI_JET_TAU',
                   'COSH_DETA': 'LUT_COSH_DETA_JET_TAU',
                   'ET0': 'LUT_JET_ET',
                   'ET1': 'LUT_TAU_ET',
                   'PREC_MASS': precision
                  } %}
  {% endif %}

{% elif objects[0].getType() == tmEventSetup.Tau %}
  {% set nEtaBits0 = scaleMap[tmGrammar.TAU + '-' + tmGrammar.ETA].getNbits() %}
  {% set nPhiBits0 = scaleMap[tmGrammar.TAU + '-' + tmGrammar.PHI].getNbits() %}
  {% set phiScale0 = scaleMap[tmGrammar.TAU + '-' + tmGrammar.PHI] %}
  {% set prefix0 = prefixTau %}
  {% if objects[1].getType() == tmEventSetup.Tau %}
    {% set nEtaBits1 = nEtaBits0 %}
    {% set nPhiBits1 = nPhiBits0 %}
    {% set phiScale1 = phiScale0 %}
    {% set prefix1 = prefix0 %}
    {% set precision = 2*scaleMap['PRECISION-' + tmGrammar.TAU + '-' + tmGrammar.TAU + '-MassPt'].getNbits()
                     +   scaleMap['PRECISION-' + tmGrammar.TAU + '-' + tmGrammar.TAU + '-Math'].getNbits() %}
    {% set LUTS = {'DPHI': 'LUT_DPHI_TAU_TAU',
                   'DETA': 'LUT_DETA_TAU_TAU',
                   'COS_DPHI': 'LUT_COS_DPHI_TAU_TAU',
                   'COSH_DETA': 'LUT_COSH_DETA_TAU_TAU',
                   'ET0': 'LUT_TAU_ET',
                   'ET1': 'LUT_TAU_ET',
                   'PREC_MASS': precision
                  } %}
  {% endif %}
{% endif %}

{# assume phiScale is the same for calo objects #}
{% set iPi = (0.5*(phiScale0.getMaximum() - phiScale0.getMinimum())/phiScale0.getStep()) | int -%}


{% if objects[0].getType() == objects[1].getType() %}
bool
{{ cond.getName() }}
(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  std::vector<int> candidates;
  for (size_t ii = 0; ii < {{prefix0}}Bx.size(); ii++)
  {
    if (not ({{prefix0}}Bx.at(ii) == {{ objects[0].getBxOffset() }})) continue;
    candidates.push_back(ii);
    {% if prefix0 == prefixTau %}
      if (candidates.size() == {{maxTau}}) break;
    {% endif %}
  }

  bool pass = false;
  if (candidates.size() < {{nObjects}}) return pass;

  std::vector<std::vector<int> > combination;
  getCombination(candidates.size(), {{nObjects}}, combination);
  std::vector<std::vector<int> > permutation;
  getPermutation({{nObjects}}, permutation);

  for (size_t ii = 0; ii < combination.size(); ii++)
  {
    const std::vector<int>& set = combination.at(ii);
    for (size_t jj = 0; jj < permutation.size(); jj++)
    {
      const std::vector<int>& indicies = permutation.at(jj);
      {{ objects | hasEtaPhiCuts }}
      const int idx0 = candidates.at(set.at(indicies.at(0)));
      const int idx1 = candidates.at(set.at(indicies.at(1)));
      {{ macros.getObjectCuts(prefix0, 'idx0', objects[0], tmEventSetup, nEtaBits0, nPhiBits0) }}
      {{ macros.getObjectCuts(prefix0, 'idx1', objects[1], tmEventSetup, nEtaBits0, nPhiBits0) }}
      {{ macros.getSameTypeCorrelationCuts(prefix0, 'idx0', 'idx1', cond, tmEventSetup, LUTS, iPi) }}
      pass = true;
      break;
    }

    if (pass) break;
  }

  return pass;
}

{% else %}
bool
{{ cond.getName() }}
(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  bool pass = false;
  {% if prefix0 == prefixTau %}
    size_t ntau = 0;
  {% endif %}

  for (size_t ii = 0; ii < {{prefix0}}Bx.size(); ii++)
  {
    if (not ({{prefix0}}Bx.at(ii) == {{ objects[0].getBxOffset() }})) continue;
    {% if prefix0 == prefixTau %}
      if (++ntau > {{maxTau}}) break;
    {% endif %}
    {{ objects | hasEtaPhiCuts }}
    {{ macros.getObjectCuts(prefix0, 'ii', objects[0], tmEventSetup, nEtaBits0, nPhiBits0) }}

    {% if prefix1 == prefixTau %}
      size_t ntau = 0;
    {% endif %}
    for (size_t jj = 0; jj < {{prefix1}}Bx.size(); jj++)
    {
      if (not ({{prefix1}}Bx.at(jj) == {{ objects[1].getBxOffset() }})) continue;
      {% if prefix1 == prefixTau %}
        if (++ntau > {{maxTau}}) break;
      {% endif %}
      {{ macros.getObjectCuts(prefix1, 'jj', objects[1], tmEventSetup, nEtaBits1, nPhiBits1) }}
      {{ macros.getDifferentTypeCorrelationCuts(prefix0, prefix1, 'ii', 'jj', cond, tmEventSetup, LUTS, iPi) }}
      pass = true;
      break;
    }
    if (pass) break;
  }

  return pass;
}
{% endif %}

{% endblock CaloCaloCorrelationTemplate %}
{# eof #}
