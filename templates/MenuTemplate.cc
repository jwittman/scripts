{#
 # @author: Takashi MATSUSHITA
 #}
/* automatically generated from {{ menu.getName() }} with menu2lib.py */
/* https://gitlab.cern.ch/cms-l1t-utm/scripts */

#include <algorithm>
#include <map>
#include <string>
#include <sstream>

#include "menulib.hh"

// utility methods
void
getCombination(int N,
               int K,
               std::vector<std::vector<int> >& combination)
{
  std::string bitmask(K, 1);
  bitmask.resize(N, 0);

  do
  {
    std::vector<int> set;
    for (int ii = 0; ii < N; ++ii)
    {
      if (bitmask[ii]) set.push_back(ii);
    }
    combination.push_back(set);
  }
  while (std::prev_permutation(bitmask.begin(), bitmask.end()));
}


void
getPermutation(int N,
               std::vector<std::vector<int> >& permutation)
{
  std::vector<int> indicies(N);
  for (int ii = 0; ii < N; ii++) indicies.at(ii) = ii;

  do
  {
    std::vector<int> set;
    for (int ii = 0; ii < N; ++ii)
    {
      set.push_back(indicies.at(ii));
    }
    permutation.push_back(set);
  }
  while (std::next_permutation(indicies.begin(), indicies.end()));
}


{#
 # jinja templates
 #}
{% set scaleMap = menu.getScaleMapPtr() %}

{# prefix #}
{% set prefixMuon = 'data->muon' %}
{% set prefixEg = 'data->eg' %}
{% set prefixTau = 'data->tau' %}
{% set prefixJet = 'data->jet' %}
{% set prefixSum = 'data->sum' %}
{% set maxTau = 8 %}


// generate conditions
{% for name, cond in menu.getConditionMapPtr().iteritems() %}
  {% if cond.getType() in (tmEventSetup.SingleMuon, tmEventSetup.DoubleMuon, tmEventSetup.TripleMuon, tmEventSetup.QuadMuon,
                           tmEventSetup.SingleEgamma, tmEventSetup.DoubleEgamma, tmEventSetup.TripleEgamma, tmEventSetup.QuadEgamma,
                           tmEventSetup.SingleTau, tmEventSetup.DoubleTau, tmEventSetup.TripleTau, tmEventSetup.QuadTau,
                           tmEventSetup.SingleJet, tmEventSetup.DoubleJet, tmEventSetup.TripleJet, tmEventSetup.QuadJet) %}
    {% include 'ObjectTemplate.cc' %}

  {% elif cond.getType() in (tmEventSetup.TotalEt, tmEventSetup.TotalHt, tmEventSetup.MissingEt, tmEventSetup.MissingHt,
                             tmEventSetup.TotalEtEM, tmEventSetup.MissingEtHF,
                             tmEventSetup.MinBiasHFP0, tmEventSetup.MinBiasHFP1,
                             tmEventSetup.MinBiasHFM0, tmEventSetup.MinBiasHFM1,
                             tmEventSetup.TowerCount) %}
    {% include 'EsumTemplate.cc' %}

  {% elif cond.getType() == tmEventSetup.Externals %}
    {% include 'Externals.cc' %}

  {% elif cond.getType() in (tmEventSetup.MuonMuonCorrelation,) %}
    {% include 'MuonMuonCorrelationTemplate.cc' %}

  {% elif cond.getType() in (tmEventSetup.MuonEsumCorrelation, tmEventSetup.CaloEsumCorrelation) %}
    {% include 'EsumCorrelationTemplate.cc' %}

  {% elif cond.getType() in (tmEventSetup.CaloMuonCorrelation,) %}
    {% include 'CaloMuonCorrelationTemplate.cc' %}

  {% elif cond.getType() in (tmEventSetup.CaloCaloCorrelation,) %}
    {% include 'CaloCaloCorrelationTemplate.cc' %}

  {% elif cond.getType() in (tmEventSetup.InvariantMass,) %}
    {% set objects = cond.getObjects() %}
    {% set combination = tmEventSetup.getObjectCombination(objects[0].getType(), objects[1].getType()) %}
    {% if combination == tmEventSetup.MuonMuonCombination %}
      {% include 'MuonMuonCorrelationTemplate.cc' %}
    {% elif combination == tmEventSetup.CaloMuonCombination %}
      {% include 'CaloMuonCorrelationTemplate.cc' %}
    {% elif combination == tmEventSetup.CaloCaloCombination %}
      {% include 'CaloCaloCorrelationTemplate.cc' %}
    {% endif %}

  {% endif %}
{% endfor %}


// generate algorithms
{% for name, algo in menu.getAlgorithmMapPtr().iteritems() %}
bool
{{ name }}(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  return {{ algo.getExpressionInCondition() | toCpp }};
}
{% endfor %}


std::string getNameFromId(const int index)
{
  static const std::pair<int, std::string> id2name[] = {
    {% for name, algo in menu.getAlgorithmMapPtr().iteritems() %}
      std::make_pair({{ algo.getIndex() }}, "{{ name }}"){% if not loop.last %},{% endif %}
    {% endfor %}
  };

  static const std::map<int, std::string> Id2Name(id2name, id2name + sizeof(id2name) / sizeof(id2name[0]));
  const std::map<int, std::string>::const_iterator rc = Id2Name.find(index);
  std::string name;
  if (rc != Id2Name.end()) name = rc->second;
  return name;
}


int getIdFromName(const std::string& name)
{
  static const std::pair<std::string, int> name2id[] = {
    {% for name, algo in menu.getAlgorithmMapPtr().iteritems() %}
      std::make_pair("{{ name }}", {{ algo.getIndex() }}){% if not loop.last %},{% endif %}
    {% endfor %}
  };

  static const std::map<std::string, int> Name2Id(name2id, name2id + sizeof(name2id) / sizeof(name2id[0]));
  const std::map<std::string, int>::const_iterator rc = Name2Id.find(name);
  int id = -1;
  if (rc != Name2Id.end()) id = rc->second;
  return id;
}


AlgorithmFunction getFuncFromId(const int index)
{
  static const std::pair<int, AlgorithmFunction> id2func[] = {
    {% for name, algo in menu.getAlgorithmMapPtr().iteritems() %}
      std::make_pair({{ algo.getIndex() }}, &{{ name }}){% if not loop.last %},{% endif %}
    {% endfor %}
  };

  static const std::map<int, AlgorithmFunction> Id2Func(id2func, id2func + sizeof(id2func) / sizeof(id2func[0]));
  const std::map<int, AlgorithmFunction>::const_iterator rc = Id2Func.find(index);
  AlgorithmFunction fp = 0;
  if (rc != Id2Func.end()) fp = rc->second;
  return fp;
}


AlgorithmFunction getFuncFromName(const std::string& name)
{
  static const std::pair<std::string, AlgorithmFunction> name2func[] = {
    {% for name, algo in menu.getAlgorithmMapPtr().iteritems() %}
      std::make_pair("{{ name }}", &{{ name }}){% if not loop.last %},{% endif %}
    {% endfor %}
  };

  static const std::map<std::string, AlgorithmFunction> Name2Func(name2func, name2func + sizeof(name2func) / sizeof(name2func[0]));
  const std::map<std::string, AlgorithmFunction>::const_iterator rc = Name2Func.find(name);
  AlgorithmFunction fp = 0;
  if (rc != Name2Func.end()) fp = rc->second;
  if (fp == 0)
  {
    std::stringstream ss;
    ss << "fat> algorithm '" << name << "' is not defined in {{ menu.getName() }}\n";
    throw std::runtime_error(ss.str());
  }
  return fp;
}


bool addFuncFromName(std::map<std::string, std::function<bool()>> &L1SeedFun,
                     L1Analysis::L1AnalysisL1UpgradeDataFormat* ntuple_)
{
  static const std::pair<std::string, AlgorithmFunction> name2func[] = {
    {% for name, algo in menu.getAlgorithmMapPtr().iteritems() %}
      std::make_pair("{{ name }}", &{{ name }}){% if not loop.last %},{% endif %}
    {% endfor %}
  };

  for (auto pair : name2func)
  {
    L1SeedFun[pair.first] = std::bind(pair.second, ntuple_);
  }

  return true;
}

// eof
