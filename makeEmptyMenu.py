#!/bin/env python
#
# @author: Takashi MATSUSHITA
#

import argparse

import tmTable


def getEmptyMenu(base):
  empty = tmTable.Menu()
  for key, val in base.menu.iteritems():
    empty.menu[key] = val

  empty.menu['ancestor_id'] = '0'
  empty.menu['menu_id'] = '0'
  empty.menu['comment'] = ''
  empty.menu['is_valid'] = '0'
  empty.menu['n_modules'] = '0'
  empty.menu['name'] = 'empty menu'
  empty.menu['uuid_firmware'] = '00000000-0000-0000-0000-000000000000'
  empty.menu['uuid_menu'] = '00000000-0000-0000-0000-000000000000'

  return empty


if __name__ == '__main__':
  xml = 'L1Menu_Collisions2016_v5.xml'
  output = 'NoAlgo.xml'

  parser = argparse.ArgumentParser()

  parser.add_argument("--menu", dest="xml", default=xml, type=str, action="store", required=True, help="path to the level1 trigger menu xml file")
  parser.add_argument("--output", dest="output", default=output, type=str, action="store", help="empty trigger menu xml file")

  options = parser.parse_args()

  table_menu = tmTable.Menu()
  table_scale = tmTable.Scale()
  table_ext = tmTable.ExtSignal()
  message = tmTable.xml2menu(options.xml, table_menu, table_scale, table_ext, False)
  if message:
    print 'error> {0}: {1}'.format(options.xml, message)

  empty = getEmptyMenu(table_menu)
  tmTable.menu2xml(empty, table_scale, table_ext, options.output);

# eof
